/* 
 * Author: Dave
 */

#ifndef AMAZINGWINDOW_H
#define	AMAZINGWINDOW_H

#include <memory>
#include <gtkmm.h>

#include "Album.h"

/**
 * Represents our window, inheriting from Gtk::Window.
 */
class AmazingWindow : public Gtk::Window
{
    public:
        AmazingWindow();  // Constructor
        ~AmazingWindow(); // Destructor
        
        // Accessor for the album
        Album& getAlbum() const;

    private:
        // Called to handle button presses
        void nextBtnHandler();
        void prevBtnHandler();

        // Contains image information
        std::auto_ptr<Album> album;
        
        // Window structure. A "VBox" is a container of widgets, which arranges its contents 
        // vertically. An "HButtonBox" arranges a set of buttons horizontally.
        Gtk::VBox vbox; 
        Gtk::HButtonBox toolbar;
        
        // Important GUI widgets. The two Gtk::Button widgets provide the "previous" and "next" 
        // controls. Gtk::Image displays the image. Gtk::Label shows the image caption.
        Gtk::Button nextBtn;
        Gtk::Button prevBtn;
        Gtk::Image image;
        Gtk::Label caption;
};

#endif
