#include "Album.h"

Album::Album() 
{
        numberImgs = 0;
}

void Album::addImage(std::string filename, std::string caption)
{
        numberImgs++;
        Image *img = new Image(filename, caption);
        Album::vector.push_back(img);
}

Image *Album::prevImg()
{
        currImg--;
        return vector[currImg];
}

Image *Album::nextImg()
{
        currImg++;
        return vector[currImg];
}
// Insert your method definitions here for Album
