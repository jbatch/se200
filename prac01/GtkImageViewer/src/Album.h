/*
 * Represents a collection of image metadata.
 * Author: ...
 */

#ifndef ALBUM_H
#define	ALBUM_H

#include "Image.h"
#include <vector>

class Album 
{
    public:
        Album();
	void addImage(std::string, std::string);
	Image *nextImg();
	Image *prevImg();
	
	
   private:
	int currImg;
	int numberImgs;
	std::vector<Image::Image*> vector;
};

#endif
