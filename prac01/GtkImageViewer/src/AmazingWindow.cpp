#include "AmazingWindow.h"
#include <iostream>

/**
 * Constructor. We must initialise the GUI here, which includes creating and 
 * adding the GUI "widgets" and setting up callback functions.
 */
AmazingWindow::AmazingWindow() 
:   album(new Album()),  // Special C++ syntax for field initialisation: "field(constructor args)"
    image(),
    prevBtn("Previous"),
    nextBtn("Next"),
    toolbar(Gtk::BUTTONBOX_START)
{
    // Set the window title (set_title is inherited from Gtk::Window).
    set_title("Gtk Image Viewer"); 
    
    // Set up the structure of the GUI window
    add(vbox); // from Gtk::Window
    vbox.add(toolbar);
    vbox.add(image);
    vbox.add(caption);
    toolbar.add(prevBtn);
    toolbar.add(nextBtn);
    
    // Set up nextBtnHandler to be called when nextBtn is clicked, and similarly for prevBtn.
    prevBtn.signal_clicked().connect(sigc::mem_fun(*this, &AmazingWindow::prevBtnHandler));
    nextBtn.signal_clicked().connect(sigc::mem_fun(*this, &AmazingWindow::nextBtnHandler));

        
    // Fix this code so that it actually displays the initial (first) image.
    image.set("[Initial image filename]");
    caption.set_text("[Initial image caption]");
    
    
    // Tell all widgets to display themselves (inherited from Gtk::Window).
    show_all();
}

/**
 * Destructor. Just for show, really. (If we were using C-style pointers, this
 * is where we'd delete (free) them.)
 */
AmazingWindow::~AmazingWindow()
{    
    
}

/**
 * Retrieves the album by reference.
 * (nb. If we'd left off the '&', this would return a copy instead.)
 */
Album& AmazingWindow::getAlbum() const
{
    return *album;
}

/**
 * Called to handle "previous" button clicks.
 */
void AmazingWindow::prevBtnHandler()
{
        Image *img = album->prevImg();
    // Fix this code so that it actually displays the previous image.
    image.set(img->getFilename());
    caption.set_text(img->getCaption());
}

/**
 * Called to handle "next" button clicks.
 */
void AmazingWindow::nextBtnHandler()
{
        Image *img = album->nextImg();
    // Fix this code so that it actually displays the next image.
    image.set(img->getFilename());
    caption.set_text(img->getCaption());
}

