import java.util.*;

/**
 * Contains all the address book entries.
 * 
 * @author Josh Batchelor 2014
 */
public class AddressBook
{
  private HashMap<String, Entry> addressBook;

  public AddressBook()
  {
    addressBook = new HashMap<String, Entry>();
  }

  public HashMap<String, Entry> getAddressBook()
  {
    return addressBook;
  }

  public void addEntry(String string, String entry)
  {
    Entry e = new Entry(entry);
    addressBook.put(string, e);
  }

  public void addEntry(String string, String []entries)
  {
    Entry e = new Entry(entries);
    addressBook.put(string, e);
  }

  public String getEntry(String entry)
  {
    return addressBook.get(entry).toString();
  }
}
